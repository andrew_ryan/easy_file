## easty file system

```rust

use easy_file::*;
fn main() {
    
// create file and write data
match  create_write_file!(std::path::Path::new("./demo.txt"),"demo".as_bytes()){
    Ok(_r)=>{
        println!("done");
    },
    Err(_e)=>{}
}

// read file return Result<Vec<u8>, Box<dyn std::error::Error>>
match  read_file!("./demo.txt"){
    Ok(_r)=>{
        println!("{:?}",_r);
    },
    Err(_e)=>{}
}


// remove file or folder
match  remove!("./test.txt"){
     Ok(_r)=>{
         println!("done");
     },
     Err(_e)=>{}
}

// list directory return Vec<PathBuf>
match  list_dir!("./"){
     Ok(_r)=>{
         println!("{:?}",r);
     },
     Err(_e)=>{}
}

//append data to file
match append_to_file!("./foo.txt", "demo".as_bytes()) {
    Ok(_r) => {
            println!("done");
    }
    Err(_e) => {}
}

//read text file return String
if let Ok(_r) = read_to_string!("./foo.txt") {
        println!("{}", _r);
}

// copy file from a path to another path
if let Ok(_r) = copy_to_path!("./src/foo.txt", "./demo.txt") {}

// move file from a path to another path
if let Ok(_r) = move_to_path!("./src/foo.txt", "./demo.txt") {}

// Creates a new, empty directory at the provided path
if let Ok(_r) = create_dir!("./demo") {}
    
// Recursively create a directory and all of its parent components if they are missing.
if let Ok(_r) = create_dir_all!("./rust/js") {}



}


```