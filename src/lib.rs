#[macro_use]
pub mod macros {
    pub type IoError<T> = Result<T, Box<dyn std::error::Error>>;
    /// ## create and write to file
    /// ```ignore
    ///if let Ok(_r) = create_write_file!("./demo.txt", "alen".as_bytes()) {
    ///    println!("{:?}", _r);
    ///}
    /// ```
    #[macro_export]
    macro_rules! create_write_file {
        ( $path:expr,$data:expr ) => {{
            use std::io::Write;
            let mut file = std::fs::File::create($path).expect("create failed");
            file.write_all(&$data)
        }};
    }

    /// ## read file return Result<Vec\<u8\>, Box<dyn std::error::Error\>\>
    /// ```ignore
    ///if let Ok(_r) = read_file!("./demo.txt") {
    ///    println!("{:?}", _r);
    ///}
    /// ```
    #[macro_export]
    macro_rules! read_file {
        ( $path:expr) => {{
            std::fs::read($path)
        }};
    }

    /// ## rename file
    /// ```ignore
    ///   match rename!("./demo.txt","./test.txt"){
    ///   Ok(_r)=>{
    ///       println!("done");
    ///   },
    ///   Err(_e)=>{}
    ///   }
    /// ```
    #[macro_export]
    macro_rules! rename {
        ($path_1:expr,$path_2:expr) => {{
            use easy_file::macros::IoError;
            use std::fs;
            use std::path::Path;
            fn rename(path_1: &Path, path_2: &Path) -> IoError<()> {
                fs::rename(path_1, path_2)?;
                Ok(())
            }
            let path_1 = Path::new($path_1);
            let path_2 = Path::new($path_2);
            rename(path_1, path_2)
        }};
    }

    /// can be used to remove file or folder
    /// ```ignore
    ///  match  remove!("./test.txt"){
    ///      Ok(_r)=>{
    ///          println!("done");
    ///      },
    ///      Err(_e)=>{}
    ///  }
    /// ```
    #[macro_export]
    macro_rules! remove {
        ($path:expr) => {{
            use easy_file::macros::IoError;
            use std::fs;
            use std::path::Path;
            fn rm(path: &Path) -> IoError<()> {
                if path.is_dir() {
                    fs::remove_dir_all(path)?;
                } else if path.is_file() {
                    fs::remove_file(path)?;
                }
                Ok(())
            }
            let path = Path::new($path);
            rm(path)
        }};
    }

    /// list directory return Vec<PathBuf>
    ///```ignore
    /// match  list_dir!("./"){
    ///    Ok(_r)=>{
    ///        println!("{:?}",_r);
    ///    },
    ///    Err(_e)=>{}
    ///}
    /// ```
    #[macro_export]
    macro_rules! list_dir {
        ($path:expr) => {{
            fn list_dir(dir: &str) -> Vec<std::path::PathBuf> {
                let files: Vec<_> = std::fs::read_dir(dir)
                    .unwrap()
                    .map(|entry| entry.unwrap().path())
                    .collect();
                files
            }
            list_dir($path)
        }};
    }

    /// append data to file
    /// ```ignore
    ///    match append_to_file!("./foo.txt", "demon".as_bytes()) {
    ///        Ok(_r) => {
    ///                println!("done");
    ///        }
    ///        Err(_e) => {}
    ///    }
    /// ```
    #[macro_export]
    macro_rules! append_to_file {
        ($path:expr,$data:expr) => {{
            use std::io::Write;
            fn append(file_path: &str, data: &[u8]) -> Result<usize, std::io::Error> {
                use std::fs::OpenOptions;
                use std::io::Read;
                let mut file_0 = OpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .open(file_path);
                let mut bytes = Vec::new();
                let file_1 = OpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .open(file_path);
                file_0.unwrap().read_to_end(&mut bytes).unwrap();
                bytes.extend(data);
                file_1.unwrap().write(&bytes.to_vec())
            }
            append($path, $data)
        }};
    }

    /// list directory return Vec<PathBuf>
    ///```ignore
    ///  if let Ok(_r) = read_to_string!("./foo.txt") {
    ///     println!("{}", _r);
    /// }
    /// ```
    #[macro_export]
    macro_rules! read_to_string {
        ($path:expr) => {{
            use easy_file::macros::IoError;
            use std::{
                fs::File,
                io::{self, Read},
                path::Path,
            };
            fn read_to_string<P: AsRef<Path>>(path: P) -> IoError<String> {
                fn inner(path: &Path) -> IoError<String> {
                    let mut file = File::open(path)?;
                    let mut string = String::new();
                    file.read_to_string(&mut string)?;
                    Ok(string)
                }
                inner(path.as_ref())
            }
            read_to_string($path)
        }};
    }
    /// copy file from a path to another path
    ///```ignore
    ///   if let Ok(_r) = copy_to_path!("./foo.txt", "./src/foo.txt") {
    ///     println!("{}", _r);
    ///  }
    /// ```
    #[macro_export]
    macro_rules! copy_to_path {
        ($from:expr,$to:expr) => {{
            use std::path::Path;
            let path_0 = Path::new($from);
            let path_1 = Path::new($to);
            std::fs::copy(path_0, path_1)
        }};
    }
    /// move file from a path to another path
    ///```ignore
    ///   if let Ok(_r) = move_to_path!("./foo.txt", "./src/foo.txt") {
    ///  }
    /// ```
    #[macro_export]
    macro_rules! move_to_path {
        ($from:expr,$to:expr) => {{
            use std::path::Path;
            let path_0 = Path::new($from);
            let path_1 = Path::new($to);
            copy_to_path!(path_0, path_1).unwrap();
            remove!(path_0).unwrap()
        }};
    }
    /// Creates a new, empty directory at the provided path
    ///```ignore
    /// if let Ok(_r) = create_dir!("./demo") {}
    /// ```
    #[macro_export]
    macro_rules! create_dir {
        ($folder_name:expr) => {{
            use easy_file::macros::IoError;
            use std::path::Path;
            let path_0 = Path::new($folder_name);
            fn create_dir(name: &Path) -> IoError<()> {
                std::fs::create_dir(name)?;
                Ok(())
            }
            create_dir(path_0)
        }};
    }

    /// Recursively create a directory and all of its parent components if they are missing.
    ///```ignore
    /// if let Ok(_r) = create_dir_all!("./rust/js") {}
    /// ```
    #[macro_export]
    macro_rules! create_dir_all {
        ($folder_name:expr) => {{
            use easy_file::macros::IoError;
            use std::path::Path;
            let path_0 = Path::new($folder_name);
            fn create_dir_all(name: &Path) -> IoError<()> {
                std::fs::create_dir_all(name)?;
                Ok(())
            }
            create_dir_all(path_0)
        }};
    }
}
