#[allow(warnings)]
use easy_file::*;
#[allow(warnings)]
fn main() {
    // run();
}
#[allow(warnings)]
fn run() {
    if let Ok(_r) = create_write_file!("./demo.txt", "alen".as_bytes()) {
        println!("{:?}", _r);
    }

    if let Ok(_r) = read_file!("./demo.txt") {
        println!("{:?}", _r);
    }
    if let Ok(_r) = read_to_string!("./demo.txt") {
        println!("{:?}", _r);
    }

    if let Ok(_r) = append_to_file!("./demo.txt", "oko".as_bytes()) {
        println!("{:?}", _r);
    }
    if let Ok(_r) = rename!("./demo.txt", "test.txt") {
        println!("{:?}", _r);
    }

    if let Ok(_r) = remove!("./test.txt") {
        println!("{:?}", _r);
    }

    if let Ok(_r) = create_dir!("./demo") {}

    if let Ok(_r) = create_dir_all!("./rust/js") {}
}
